#......................................................#
#1.- ANOTAR EL TIEMPO AL INICIO

init <- Sys.time()

#......................................................#
# 2.-VECTOR DE ACTIVIDADES

activity   <- c("WALKING","WALKING_UPSTAIRS","WALKING_DOWNSTAIRS","SITTING","STANDING","LAYING")

#......................................................#
# 3.- LEER FICHERO FEATURES

path_train <- "C:/Users/Jose Antonio Rios/Desktop/Datos_Coursera2/UCI HAR Dataset/"
file_features <- "features.txt"

fich_features <- paste(path_train,file_features,sep ="")
df_features   <- read.csv(fich_features, sep="",dec=".",header=FALSE,encoding = "UTF-8")
z <- as.vector(df_features$V2)

#......................................................#
# 4.-LEER FICHEROS TRAIN (Data, Activity and Subject) ==> CREA DATA.FRAME TRAIN

path_train <- "C:/Users/Jose Antonio Rios/Desktop/Datos_Coursera2/UCI HAR Dataset/train/"

file_train_x <- "X_train.txt"
file_train_y <- "y_train.txt"
file_train_s <- "subject_train.txt"

fich_train_x <- paste(path_train,file_train_x,sep ="")
df_train_x   <- read.csv(fich_train_x, sep="",dec=".",header=FALSE,encoding = "UTF-8")

fich_train_y <- paste(path_train,file_train_y,sep ="")
df_train_y   <- read.csv(fich_train_y, sep="",dec=".",header=FALSE,encoding = "UTF-8")
colnames(df_train_y)  <- c("Actividad")

fich_train_s <- paste(path_train,file_train_s,sep ="")
df_train_s   <- read.csv(fich_train_s, sep="",dec=".",header=FALSE,encoding = "UTF-8")
colnames(df_train_s)  <- c("Sujeto")
df_train <- cbind(df_train_s,df_train_y,df_train_x)

#......................................................#
# 5.- LEER FICHEROS TREST (Data, Activity and Subject) ==> CREA DATA.FRAME TEST

path_test <- "C:/Users/Jose Antonio Rios/Desktop/Datos_Coursera2/UCI HAR Dataset/test/"

file_test_x <- "X_test.txt"
file_test_y <- "y_test.txt"
file_test_s <- "subject_test.txt"

fich_test_x <- paste(path_test,file_test_x,sep ="")
df_test_x   <- read.csv(fich_test_x, sep="",dec=".",header=FALSE,encoding = "UTF-8")

fich_test_y <- paste(path_test,file_test_y,sep ="")
df_test_y   <- read.csv(fich_test_y, sep="",dec=".",header=FALSE,encoding = "UTF-8")
colnames(df_test_y)  <- c("Actividad")

fich_test_s <- paste(path_test,file_test_s,sep ="")
df_test_s   <- read.csv(fich_test_s, sep="",dec=".",header=FALSE,encoding = "UTF-8")
colnames(df_test_s)  <- c("Sujeto")

df_test <- cbind(df_test_s,df_test_y,df_test_x)

#......................................................#
#6.- UNIR FICHEROS TRAIN y TEST ==> DATA.FRAME TOTAL_AO (ORDENADO POR ACTIVIDAD)

df_TOTAL <- rbind(df_train,df_test)
colnames(df_TOTAL) <- c("Subject", "Activity",z) 

df_TOTAL_OA <- df_TOTAL[order(df_TOTAL$Activity),]

#......................................................#
# 7.- CREAR SUBSET Y MEDIA Y SD PPOR ACTIVIDAD ==> CREA DATA.FRAMES 
# DF_A_M (actividad-media) y DF_A_D(actividad - sd)

lista_a_m <- list()
lista_a_d <- list()
    
for (a in 1:length(activity)){
    q <- subset(df_TOTAL_OA,df_TOTAL_OA$Activity == a)
    lista_a_m[a] <- list(c(apply(q[,-(1:2)],2,mean)))
    lista_a_d[a] <- list(c(apply(q[,-(1:2)],2,sd)))
    if (a == 1) {
        df_a_m <- data.frame(rbind(lista_a_m[[1]]))
        df_a_d <- data.frame(rbind(lista_a_d[[1]]))
        }
        else { 
        df_a_m <- data.frame(rbind(df_a_m,lista_a_m[[a]]))
        df_a_d <- data.frame(rbind(df_a_d,lista_a_d[[a]]))
        }
}

df_a_mean <- data.frame(cbind("activity"=activity,df_a_m))
df_a_sd   <- data.frame(cbind("activity"=activity,df_a_d))

#......................................................#
# 8.- CALCULO DE LA MEDIA POR ACTIVIDAD Y SUJETO

df_TOTAL_SA <- df_TOTAL_OA[order(df_TOTAL_OA$Activity,df_TOTAL_OA$Subject),]

subject <- as.numeric(names(table(df_TOTAL_OA$Subject)))


lista_a_s <- list()
lista_a   <- list()
df_a_s_TOTAL <- data.frame()

lista_a   <- activity

for (a in 1:length(activity)){
    q <- subset(df_TOTAL_OA,df_TOTAL_OA$Activity == a)
    for (s in 1:length(subject)){
        qs <- subset(q,q$Subject == s)
        lista_a_s[s] <- list(c(apply(qs[,-(1:2)],2,mean)))
        if (s == 1) {
            df_a_s <- data.frame(rbind(lista_a_s[[1]]))
        }
        else { 
            df_a_s <- data.frame(rbind(df_a_s,lista_a_s[[s]]))
        }
    }
    df_a_subject <- data.frame(cbind("Actvity"=lista_a[[a]],"Subject" = 1:30,df_a_s))
    df_a_s_TOTAL <- data.frame(rbind(df_a_s_TOTAL,df_a_subject))
    
}

#......................................................#
# 9.- GUARDAR FICHEROS

path_save   <- "C:/Users/Jose Antonio Rios/Desktop/BIGDATA/P_GIT/"

# DATA_SET TRAIN + TEST

fich1       <- "Train_and_Test.txt"
data_file_1 <-  paste(path_save,fich1,sep=" ")

write.table(df_TOTAL_OA,file=data_file_1,row.name=FALSE)

# DATA_SET MEAN FOR ACTIVITY

fich2       <- "Mean__for_Activity.txt"
data_file_2 <-  paste(path_save,fich2,sep=" ")

write.table(df_a_mean,file=data_file_2,row.name=FALSE)

# DATA_SET SD FOR ACTIVITY

fich3       <- "Sd_for_Activity.txt"
data_file_3 <-  paste(path_save,fich3,sep=" ")

write.table(df_a_sd,file=data_file_3,row.name=FALSE)

# DATA_SET MEAN FOR ACTIVITY and SUBJECT

fich4       <- "Mean_Activity_Subject.txt"
data_file_4 <-  paste(path_save,fich4,sep=" ")

write.table(df_a_s_TOTAL,file=data_file_4,row.name=FALSE)




#......................................................#
#10.- CALCULO TIEMPO PGM

end <- Sys.time()

elapsed <- paste("elapsed time for pgm   :", end-init, "  seconds")

print(elapsed)


