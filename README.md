

The file run_analysis.R is structured :



1.- ANOTAR EL TIEMPO AL INICIO(read time init)
2.- VECTOR DE ACTIVIDADES (activity vector)
3.- LEER FICHERO FEATURES (read file Features)
4.- LEER FICHEROS TRAIN (Data, Activity and Subject) ==> CREA DATA.FRAME TRAIN (read train file)
5.- LEER FICHEROS TRAIN (Data, Activity and Subject) ==> CREA DATA.FRAME TEST  (read test file)
6.- UNIR FICHEROS TRAIN y TEST ==> DATA.FRAME TOTAL_AO (ORDENADO POR ACTIVIDAD)  (join train and test files)
7.- CREAR SUBSET Y MEDIA Y SD PPOR ACTIVIDAD ==> CREA DATA.FRAMES (mean and sd for activity)
    DF_A_M (actividad-media) y DF_A_D(actividad - sd)
8.- CALCULO DE LA MEDIA POR ACTIVIDAD Y SUJETO (mean for activity and subject)
9.- GUARDAR FICHEROS (save files of results)
				# DATA_SET TRAIN + TEST
				# DATA_SET MEAN FOR ACTIVITY
				# DATA_SET SD FOR ACTIVITY
				# DATA_SET MEAN FOR ACTIVITY and SUBJECT

10.- CALCULO TIEMPO PGM(calcul elapsed time)

